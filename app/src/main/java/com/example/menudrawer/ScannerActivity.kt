package com.example.menudrawer

import android.Manifest
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.menudrawer.botones.TicketZara
import com.example.menudrawer.databinding.ActivityScannerBinding
import com.github.florent37.runtimepermission.RuntimePermission
import com.lucem.anb.characterscanner.Scanner
import com.lucem.anb.characterscanner.ScannerListener
import java.io.File
import java.io.IOException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*

//DECLARAR VARIABLE GLOBAL
var a:String ?= null

class ScannerActivity : AppCompatActivity() {
    private lateinit var file : File
    var currentPhotoPath: String = ""
    val REQUEST_IMAGE_CAPTURE = 1
    private lateinit var binding: ActivityScannerBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityScannerBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        var extractValue:String?= ""
            //Esconder los botones de las tiendas
            binding.btnZara.visibility = View.GONE


        val scanner = Scanner(this,binding.surfaceView,object : ScannerListener {
            override fun onDetected(texto: String?) {
                extractValue = texto

            }

            override fun onStateChanged(texto: String?, p1: Int) {
                if (texto != null) {
                    Log.d("check state", texto)
                }
            }

        })


        //Al pulsar al boton comprueba que se haya escaneado y mantiene visible el editor de texto.
        binding.btnCapture.setOnClickListener {
            // al pulsar el boton capturar de dejan de ver Surface y el boton y aparace Edit(text)
            // Muestra los botones y esconde el de escanear
            scanner.isScanning = false
            binding.surfaceView.visibility = View.GONE
            binding.btnCapture.visibility = View.GONE
            binding.btnZara.visibility = View.VISIBLE
            binding.edtValue.visibility = View.VISIBLE
            binding.btnGaleria.visibility = View.GONE
            binding.edtValue.setText(extractValue)


        }
        binding.btnGaleria?.setOnClickListener {
            RuntimePermission.askPermission(this)
                .request(Manifest.permission.CAMERA)
                .onAccepted {
                    val intent = Intent(this, guardarImagen::class.java)
                    startActivity(intent)
                }
                .onDenied {
                    Toast.makeText(this, "Por favor acepta los permisos", Toast.LENGTH_LONG).show()
                    it.askAgain()
                }
                .ask()
        }

        binding.btnZara.setOnClickListener {
            a = extractValue
            if (a!!.contains("ZARA")|| a!!.contains("PULL")||a!!.contains("BERSHKA")) {
                val intent = Intent(this, TicketZara::class.java)
                startActivity(
                    intent
                )
            } else {
                Toast.makeText(this, "Este tickets no es de nuestras marcas", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, ScannerActivity::class.java)
                startActivity(
                    intent)
            }
        }

    }

    override fun onBackPressed() {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("¿Esta seguro de eliminar?")
            builder.setMessage("Aviso")
            builder.setPositiveButton("Si") {
                    dialogInterface, i -> dialogInterface.dismiss()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(
                    intent
                )
            }

            builder.setNegativeButton("No") { dialogInterface, i ->
                dialogInterface.dismiss()
            }

            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        }

}