package com.example.menudrawer

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import com.example.menudrawer.databinding.ActivityLoginBinding
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var binding:ActivityLoginBinding
    private lateinit var progressDialog:ProgressDialog
    private lateinit var firebaseAuth:FirebaseAuth
    private var email = ""
    private var password = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Configurar la barra de progreso

        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Espere")
        progressDialog.setMessage("Cargando...")
        progressDialog.setCanceledOnTouchOutside(false)

        //Inicializar firebase
        firebaseAuth = FirebaseAuth.getInstance()
        comprobrarUsuario()

        binding.noAccountTv.setOnClickListener {
            startActivity(Intent(this, RegistroActivity::class.java))
        }


        binding.loginBtn.setOnClickListener {
            comprobarDatos()
        }
    }

    private fun comprobarDatos()
    {
        // Comprueba que los datos introducidos sean correctos
        email = binding.emailEt.text.toString().trim()
        password = binding.passwordEt.text.toString().trim()

        // Comprueba que el formato del email sea correcto
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            binding.emailEt.error = "Formato del email invalido"
        }
        else if (TextUtils.isEmpty(password))
        {
            binding.passwordEt.error = "Por favor introduce la contraseña"
        }
        else
        {
            firebaseLogin()
        }

    }

    private fun firebaseLogin()
    {
        progressDialog.show()
        firebaseAuth.signInWithEmailAndPassword(email,password)
            .addOnSuccessListener {
                progressDialog.dismiss()

                val user = firebaseAuth.currentUser
                val email = user!!.email

                Toast.makeText(this, "Bienvenido $email", Toast.LENGTH_SHORT).show()
                startActivity(Intent(this, MainActivity::class.java))
                dineroGastado = 0.0
                nVecesZara = ""
                nVecesPull = ""
                nVecesBershka = ""
                tiendaFav = ""
                nCamisetas = ""
                nPantalones = ""
                finish()
            }
            .addOnFailureListener { e->
                progressDialog.dismiss()
                Toast.makeText(this, "Error al iniciar sesión ${e.message}", Toast.LENGTH_SHORT).show()
            }

    }

    private fun comprobrarUsuario()
    {
        // Comprueba si el usuario ya esta logeado y recupera el nombre de usuario
        val user = firebaseAuth.currentUser

        if (user != null)
        {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }



}