package com.example.menudrawer

import android.content.Context
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.menudrawer.adapter.ImagenAdapter
import com.example.menudrawer.databinding.ActivityGuardarImagenBinding
import com.example.menudrawer.databinding.ActivityImagenAmpliadaBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.squareup.picasso.Picasso

class imagenAmpliada : AppCompatActivity() {
    private lateinit var binding: ActivityImagenAmpliadaBinding
    private lateinit var firebaseAuth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImagenAmpliadaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firebaseAuth = FirebaseAuth.getInstance()


        var bundle : Bundle ?= intent.extras
        var  url = bundle!!.getString("url")
        Picasso.get().load(url).into(binding.tvimagenAmpliada)

//        eliminar()

    }

//    fun eliminar(){
//        binding.btnEliminar.setOnClickListener {
//            val imageRef = Firebase.storage.reference
//            val user = firebaseAuth.currentUser
//            val email = user?.email
//            var path = email.toString()            // aqui hay que poner el path de la imagen
//
//            val imgRef = imageRef.child(path)
//
//
//            imgRef.delete().addOnSuccessListener {
//
//                Toast.makeText(this, "Imagen eliminada con exito", Toast.LENGTH_SHORT)
//            }
//                .addOnFailureListener {
//
//                    Toast.makeText(this, "No se ha podido eliminar la imagen", Toast.LENGTH_SHORT)
//
//                }
//        }
//    }

}

