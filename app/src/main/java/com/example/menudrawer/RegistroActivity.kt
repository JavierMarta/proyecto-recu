package com.example.menudrawer

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Toast
import com.example.menudrawer.databinding.ActivityRegistroBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class RegistroActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegistroBinding
    private lateinit var progressDialog: ProgressDialog
    private lateinit var firebaseAuth: FirebaseAuth
    private val db = FirebaseFirestore.getInstance()
    private var email = ""
    private var password = ""
    private var nombre = ""
    private var apellido = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegistroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Espere")
        progressDialog.setMessage("Creando usuario...")
        progressDialog.setCanceledOnTouchOutside(false)

        firebaseAuth = FirebaseAuth.getInstance()

        binding.signUpBtn.setOnClickListener {
            comprobarDatos()
        }


    }

    private fun agregarUsuario() {
        nombre = binding.nombreEt.text.toString().trim()
        apellido = binding.apellidoEt.text.toString().trim()
        email = binding.emailEt.text.toString().trim()
        password = binding.passwordEt.text.toString().trim()

        db.collection("users").document(email).set(
            hashMapOf(
                "nombre" to nombre, "apellido" to apellido,
                "email" to email, "password" to password
            )
        )
    }

    private fun comprobarDatos() {
        email = binding.emailEt.text.toString().trim()
        password = binding.passwordEt.text.toString().trim()
        nombre = binding.nombreEt.text.toString().trim()
        apellido = binding.apellidoEt.text.toString().trim()

        if (TextUtils.isEmpty(nombre))
        {
            binding.nombreEt.error = "Por favor introduce tu nombre"
        }
        else if (TextUtils.isEmpty(apellido))
        {
            binding.apellidoEt.error = "Por favor introduce tu apellido"
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            binding.emailEt.error = "Formato del email invalido"
        }
        else if (TextUtils.isEmpty(password))
        {
            binding.passwordEt.error = "Por favor introduce la contraseña"
        }
        else if (password.length < 6)
        {
            binding.passwordEt.error = "La contraseña debe tener al menos 6 carácteres"
        }
        else
        {
            firebaseSignUp()
            agregarUsuario()
        }
    }

    private fun firebaseSignUp() {
        progressDialog.show()

        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                progressDialog.dismiss()

                val user = firebaseAuth.currentUser
                val email = user!!.email

                Toast.makeText(this, "Registro completado $email", Toast.LENGTH_SHORT).show()

                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
            .addOnFailureListener { e ->
                progressDialog.dismiss()
                Toast.makeText(this, "Registro fallido ${e.message}", Toast.LENGTH_SHORT).show()
            }
    }

    override fun onSupportNavigateUp(): Boolean {

        startActivity(Intent(this, LoginActivity::class.java))         // Volver al activity anterior al pulsar el boton atras del actionBar
        return super.onSupportNavigateUp()
    }

}