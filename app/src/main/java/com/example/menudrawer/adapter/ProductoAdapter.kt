package com.example.menudrawer.adapter

import android.app.AlertDialog
import android.content.Context
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.menudrawer.R
import com.example.menudrawer.provider.Producto
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class ProductoAdapter(val c: Context, private val productoList: ArrayList<Producto>) :
    RecyclerView.Adapter<ProductoAdapter.viewHolder>() {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var mlistener: onItemClickListener

    interface onItemClickListener {

        fun onItemClick(position: Int)

    }

    fun setOnItemClickListener(listener: onItemClickListener) {

        mlistener = listener

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_producto, parent, false)

        return viewHolder(itemView)

    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {

        val producto: Producto = productoList[position]

        holder.idTicket.text = "ticket " + producto.idTicket
        holder.producto.text = producto.producto
        holder.precioProducto.text = producto.precioProducto
        holder.udsProducto.text = producto.udsProducto

    }

    override fun getItemCount(): Int {

        return productoList.size
    }

    inner class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val idTicket: TextView = itemView.findViewById(R.id.tvIdTicket)
        val producto: TextView = itemView.findViewById(R.id.tvProducto)
        val precioProducto: TextView = itemView.findViewById(R.id.tvPrecioProducto)
        val udsProducto: TextView = itemView.findViewById(R.id.tvUdsProducto)
        var popupmenu: ImageView = itemView.findViewById(R.id.popupMenu)

        init {

            popupmenu = itemView.findViewById(R.id.popupMenu)
            popupmenu.setOnClickListener { popUpMenu(it) }

        }

        private fun popUpMenu(v:View) {

            val position = productoList[adapterPosition]

            val popupmenu = PopupMenu(c,v)
            popupmenu.inflate(R.menu.popup_productos)

            popupmenu.setOnMenuItemClickListener {

                when(it.itemId){

                    R.id.menu_editar -> {

                        val v = LayoutInflater.from(c).inflate(R.layout.edit_item_producto, null)
                        val producto = v.findViewById<EditText>(R.id.etProductoPop)
                        val precio = v.findViewById<EditText>(R.id.etPrecioPop)
                        val unidades = v.findViewById<EditText>(R.id.etUnidadesPop)

                        AlertDialog.Builder(c).setView(v)
                            .setPositiveButton("Ok"){

                                    dialog,_->

                                if (producto.text.isEmpty() || precio.text.isEmpty() || unidades.text.isEmpty())
                                {
                                    Toast.makeText(c, "Faltan campos por rellenar", Toast.LENGTH_LONG).show()
                                }
                                else {

                                    position.producto = producto.text.toString()
                                    position.precioProducto = precio.text.toString()
                                    position.udsProducto = unidades.text.toString()
                                    notifyDataSetChanged()

                                    modificarDatos(
                                        producto.text.toString(),
                                        precio.text.toString(),
                                        unidades.text.toString(),
                                        position.id.toString()
                                    )

                                    Toast.makeText(c, "Datos modificados", Toast.LENGTH_SHORT).show()
                                    dialog.dismiss()
                                }
                            }
                            .setNegativeButton("Cancel"){

                                    dialog,_->
                                dialog.dismiss()
                            }
                            .create().show()

                        true
                    }

                    else -> true

                }

            }
            popupmenu.show()

            val popup = PopupMenu::class.java.getDeclaredField("mPopup")
            popup.isAccessible = true

            val menu = popup.get(popupmenu)
            menu.javaClass.getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(menu, true)

        }

        private fun modificarDatos(producto: String, precio: String, unidades: String, idProducto: String) {

            db = FirebaseFirestore.getInstance()
            firebaseAuth = FirebaseAuth.getInstance()
            val user = firebaseAuth.currentUser
            val email = user?.email

            db.collection("users").document(email.toString()).collection("productos").document("producto $idProducto")
                .update("producto", producto, "precio", precio.toDouble(),"unidades", unidades.toInt())


        }

    }

    //BORRAR DATOS DESDE EL RECYCLERVIEW
    fun deleteItem(i : Int ){

        db = FirebaseFirestore.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()

        val user = firebaseAuth.currentUser
        val email = user?.email

        var itemBorrar = productoList[i].id?.lowercase()
        productoList.removeAt(i)

        db.collection("users").document(email.toString()).collection("productos").get()
            .addOnSuccessListener { documents ->

            for (document in documents)
            {
                var idProd = document.get("id").toString()
                    if (idProd == itemBorrar) {
                        idProd = "producto " + document.get("id")
                        db.collection("users").document(email.toString())
                            .collection("productos").document("${idProd}").delete()
                    }
                }

            }

        notifyDataSetChanged()

    }
    //ARCHIVAR(ARRASTRAR HASTA ABAJO)

    fun archiveItem(i : Int ){


        productoList.removeAt(i)


        notifyDataSetChanged()

    }

    fun addItem(i: Int, producto: Producto) {

        productoList.add(i, producto)
        notifyDataSetChanged()
    }
}