package com.example.menudrawer.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.menudrawer.R
import com.example.menudrawer.imagenAmpliada
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.item_imagen.view.*

class ImagenAdapter(val c : Context, val urls: List<String>): RecyclerView.Adapter<ImagenAdapter.ImageViewHolder>() {


    inner class ImageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        var imagen: ImageView = itemView.findViewById(R.id.imageView2)

        init {
            imagen.setOnClickListener {

                val position = urls[adapterPosition]

                var intent = Intent(itemView.context, imagenAmpliada::class.java)
                intent.putExtra("url", position)

            itemView.context.startActivity(intent)

                    }
                }

            }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder( LayoutInflater.from(parent.context).inflate(R.layout.item_imagen,parent,false))
    }

    override fun getItemCount(): Int {
        return urls.size
    }
    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val url = urls[position]
        Glide.with(holder.itemView).load(url).into(holder.itemView.imageView2)
    }

}