package com.example.menudrawer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.menudrawer.R
import com.example.menudrawer.provider.Ticket

class archiveAdapter (val c: Context, private val archiveList: ArrayList<Ticket>) : RecyclerView.Adapter<archiveAdapter.viewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): archiveAdapter.viewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_archivado, parent, false)

        return viewHolder(itemView)

    }

    override fun onBindViewHolder(holder: archiveAdapter.viewHolder, position: Int) {

        val ticket: Ticket = archiveList[position]

        holder.fechaTicket.text = ticket.fechaTicket
        holder.tiendaTicket.text = ticket.tiendaTicket
        holder.importeTicket.text = ticket.importeTicket.toString()

    }

    override fun getItemCount(): Int {

        return archiveList.size

    }

    inner class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var fechaTicket: TextView = itemView.findViewById(R.id.tvFechaTicket)
        var tiendaTicket: TextView = itemView.findViewById(R.id.tvTiendaTicket)
        var importeTicket: TextView = itemView.findViewById(R.id.tvImporteTicket)

    }




}