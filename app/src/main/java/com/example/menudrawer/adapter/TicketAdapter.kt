package com.example.menudrawer.adapter

import android.app.AlertDialog
import android.content.Context
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.menudrawer.R
import com.example.menudrawer.provider.Ticket
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.getField
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TicketAdapter(val c: Context, private val ticketList: ArrayList<Ticket>) :
    RecyclerView.Adapter<TicketAdapter.viewHolder>() {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var mlistener: onItemClickListener

    interface onItemClickListener {

        fun onItemClick(position: Int)

    }

    fun setOnItemClickListener(listener: onItemClickListener) {

        mlistener = listener

    }

    fun deleteArchiveItem(i: Int) {

        try{

            db = FirebaseFirestore.getInstance()
            firebaseAuth = FirebaseAuth.getInstance()

            val user = firebaseAuth.currentUser
            val email = user?.email

            val itemBorrar = ticketList[i].id?.lowercase()
            ticketList.removeAt(i)



            db.collection("users").document(email.toString()).collection("tickets").get()
                .addOnSuccessListener { documents ->

                    for (document in documents) {
                        var idTicket = document.get("id").toString()
                        if (idTicket == itemBorrar)
                        {
                            db.collection("users").document(email.toString()).collection("tickets")
                                .document("ticket $itemBorrar").delete()
                        }

                    }

                }

            notifyDataSetChanged()

        }catch (e:NumberFormatException){
            Toast.makeText(c, "Error con algún campo numerico", Toast.LENGTH_LONG).show()

        }catch (e: Exception){
            Toast.makeText(c, "Error", Toast.LENGTH_LONG).show()

        }
    }

    fun deleteItem(i: Int) {

        try{

        db = FirebaseFirestore.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()

        val user = firebaseAuth.currentUser
        val email = user?.email

        val itemBorrar = ticketList[i].id?.lowercase()
        ticketList.removeAt(i)



        db.collection("users").document(email.toString()).collection("tickets").get()
            .addOnSuccessListener { documents ->

                for (document in documents) {
                    var idTicket = document.get("id").toString()
                    if (idTicket == itemBorrar)
                    {
                        db.collection("users").document(email.toString()).collection("tickets")
                            .document("ticket $itemBorrar").delete()
                    }

                }

            }
        db.collection("users").document(email.toString()).collection("productos").get()
            .addOnSuccessListener { documents ->

                for (document in documents) {
                    var idTicket =  document.get("idTicket").toString()
                    if (idTicket == itemBorrar) {

                        db.collection("users").document(email.toString())
                            .collection("productos")
                            .whereEqualTo("idTicket", "${idTicket.toInt()}").get()
                            .addOnSuccessListener {
                                var idProd =  document.get("id").toString()
                                for (document in documents) {

                                    db.collection("users").document(email.toString())
                                        .collection("productos")
                                        .document("producto ${idProd.toInt()}").delete()
                                }
                            }
                    }

                }

            }

        notifyDataSetChanged()

    }catch (e:NumberFormatException){
            Toast.makeText(c, "Error con algún campo numerico", Toast.LENGTH_LONG).show()

        }catch (e: Exception){
            Toast.makeText(c, "Error", Toast.LENGTH_LONG).show()

        }
    }

    fun addItem(i: Int, ticket: Ticket) {

        ticketList.add(i, ticket)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_ticket, parent, false)

        return viewHolder(itemView)

    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {

        val ticket: Ticket = ticketList[position]

        holder.id.text = ticket.id
        holder.fechaTicket.text = ticket.fechaTicket
        holder.tiendaTicket.text = ticket.tiendaTicket
        holder.importeTicket.text = ticket.importeTicket.toString()

    }

    override fun getItemCount(): Int {

        return ticketList.size
    }


    inner class viewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var id: TextView = itemView.findViewById(R.id.tvId)
        var fechaTicket: TextView = itemView.findViewById(R.id.tvFechaTicket)
        var tiendaTicket: TextView = itemView.findViewById(R.id.tvTiendaTicket)
        var importeTicket: TextView = itemView.findViewById(R.id.tvImporteTicket)
        var popupmenu: ImageView = itemView.findViewById(R.id.popupMenu)

        init {
            id = itemView.findViewById(R.id.tvId)
            fechaTicket = itemView.findViewById(R.id.tvFechaTicket)
            tiendaTicket = itemView.findViewById(R.id.tvTiendaTicket)
            importeTicket = itemView.findViewById(R.id.tvImporteTicket)
            popupmenu = itemView.findViewById(R.id.popupMenu)

            popupmenu.setOnClickListener { popUpMenu(it) }

        }


        private fun popUpMenu(v: View) {

            db = FirebaseFirestore.getInstance()
            firebaseAuth = FirebaseAuth.getInstance()
            val user = firebaseAuth.currentUser
            val email = user?.email

            val position = ticketList[absoluteAdapterPosition]

            var todosProductos = ""
            var todasUnidades = ""

            db.collection("users").document(email.toString()).collection("productos")
                .whereEqualTo( "idTicket", position.id!!.toInt()).get()
                .addOnSuccessListener { documents ->
//                    Log.d("hola", "entra")

                    for (document in documents)
                    {
                        Log.d("hola", "entra")
                        todosProductos += " " + document.getField("producto")
                        todasUnidades += " " + document.get("unidades").toString()
                    }
                }

            val popupmenu = PopupMenu(c, v)
            popupmenu.inflate(R.menu.popup_menu)

            popupmenu.setOnMenuItemClickListener {

                when (it.itemId) {

                    R.id.menu_editar -> {

                        val v = LayoutInflater.from(c).inflate(R.layout.edit_item, null)
                        val fecha = v.findViewById<EditText>(R.id.etFechaPop)
                        val tienda = v.findViewById<EditText>(R.id.etTiendaPop)
                        val importe = v.findViewById<EditText>(R.id.etImportePop)

                        AlertDialog.Builder(c).setView(v)
                            .setPositiveButton("Ok") {

                                    dialog, _ ->

                                if (fecha.text.isEmpty() || tienda.text.isEmpty() || importe.text.isEmpty())
                                {
                                    Toast.makeText(c, "Faltan campos por rellenar", Toast.LENGTH_LONG).show()
                                }
                                else
                                {
                                    if (validarFecha(fecha.text.toString()))
                                    {
                                        position.fechaTicket = fecha.text.toString()
                                        position.tiendaTicket = tienda.text.toString()
                                        position.importeTicket = importe.text.toString()
                                        notifyDataSetChanged()

                                        modificarDatos(fecha.text.toString(), tienda.text.toString(), importe.text.toString(), position.id.toString())

                                        Toast.makeText(c, "Datos modificados", Toast.LENGTH_SHORT).show()
                                    }
                                    else
                                    {
                                        Toast.makeText(c, "El formato de la fecha no es correcto", Toast.LENGTH_SHORT).show()
                                    }

                                    dialog.dismiss()
                                }

                            }
                            .setNegativeButton("Cancel") {

                                    dialog, _ ->
                                dialog.dismiss()
                            }
                            .create().show()

                        true
                    }

                    R.id.menu_mostrar -> {

                        val v2 = LayoutInflater.from(c).inflate(R.layout.show_item, null)
                        val fecha = v2.findViewById<TextView>(R.id.tvFechaPop)
                        val tienda = v2.findViewById<TextView>(R.id.tvTiendaPop)
                        val productos = v2.findViewById<TextView>(R.id.tvProductosPop)
                        val unidades = v2.findViewById<TextView>(R.id.tvUnidadesPop)
                        val importe = v2.findViewById<TextView>(R.id.tvImportePop)

                        AlertDialog.Builder(c).setView(v2)
                            .show()
                        fecha.text = position.fechaTicket
                        tienda.text = position.tiendaTicket
                        productos.text = todosProductos
                        unidades.text = todasUnidades
                        importe.text = position.importeTicket

                        true
                    }

                    else -> true

                }

            }
            popupmenu.show()

            val popup = PopupMenu::class.java.getDeclaredField("mPopup")
            popup.isAccessible = true

            val menu = popup.get(popupmenu)
            menu.javaClass.getDeclaredMethod("setForceShowIcon", Boolean::class.java)
                .invoke(menu, true)

        }

        private fun validarFecha(fecha: String): Boolean {

            val regex1 = Regex("([0-9]{2})/([0-9]{2})/([0-9]{4})")
            val regex2 = Regex("([0-9]{2})-([0-9]{2})-([0-9]{4})")

            var esValido = false

            var calendar = Calendar.getInstance()
            var anioActual = calendar.get(Calendar.YEAR)
            var dia = 0
            var mes = 0
            var anio = 0

            val fechaNueva = fecha.split("/","-")
            dia = fechaNueva[0].toInt()
            mes = fechaNueva[1].toInt()
            anio = fechaNueva[2].toInt()


            if (fecha.matches(regex1) || fecha.matches(regex2))
            {
                if (dia > 31 || mes > 12 || anio > anioActual)
                {
                    Toast.makeText(c, "El formato de la fecha no es correcto", Toast.LENGTH_SHORT).show()
                    return false
                }
                else  if ((mes == 2) && (dia > 28))
                {
                    Toast.makeText(c, "El formato de la fecha no es correcto", Toast.LENGTH_SHORT).show()
                    return false
                }
                else
                {
                    esValido = true
                }
            }

            return esValido
        }

        private fun modificarDatos(fecha: String, tienda: String, importe: String, idTicket: String) {

            db = FirebaseFirestore.getInstance()
            firebaseAuth = FirebaseAuth.getInstance()
            val user = firebaseAuth.currentUser
            val email = user?.email

            db.collection("users").document(email.toString()).collection("tickets")
                .document("ticket " + idTicket.lowercase()).set(
                mapOf( "id" to idTicket,"fecha" to fecha, "tienda" to tienda.lowercase(), "precioTotal" to importe.toDouble()))
        }

    }

}
