package com.example.menudrawer

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.example.menudrawer.databinding.FragmentSettingsBinding
import com.example.menudrawer.databinding.FragmentTicketsBinding
import com.example.menudrawer.ui.home.HomeViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot


class SettingsFragment : Fragment() {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth

    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        // Inflate the layout for this fragment
        ViewModelProvider(this)[HomeViewModel::class.java]
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)

        binding.btnBorrar.setOnClickListener {
            borrar()
        }
        return binding.root
    }

    fun borrar() {

        db = FirebaseFirestore.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user?.email
        var id = ""

        val  dialogo = activity?.let { it1 -> AlertDialog.Builder(it1) }
            dialogo?.setTitle("¿Esta seguro de eliminar su usuario?")
            dialogo?.setMessage("Se eliminarán todos los datos")
            dialogo?.setPositiveButton("Si") { dialogInterface, i ->


                user!!.delete()
                    .addOnCompleteListener { task ->
                        db.collection("users").document(email.toString()).collection("tickets")
                            .get().addOnSuccessListener { documents ->

                                for (document in documents)
                                {
                                    id = document.get("id").toString()
                                    db.collection("users").document(email.toString()).collection("tickets")
                                        .document("ticket "+ id).delete()
                                }

                            }

                        db.collection("users").document(email.toString()).collection("productos")
                            .get().addOnSuccessListener { documents ->

                                for (document in documents)
                                {
                                    id = document.get("id").toString()
                                    db.collection("users").document(email.toString()).collection("productos")
                                        .document("producto " + id).delete()
                                }
                            }

                        db.collection("users").document(email.toString())
                            .collection("archivados")
                            .whereEqualTo("id", 0).get()
                            .addOnSuccessListener { documents ->

                                for (document in documents) {

                                    var idArch = document.reference.id

                                    db.collection("users").document(email.toString())
                                        .collection("archivados")
                                        .document(idArch).delete()
                                }
                            }

                        db.collection("users").document(email.toString()).delete()

                        val intent = Intent(context, LoginActivity::class.java)
                        startActivity(intent)
                        Toast.makeText(activity, "Eliminado correctamente", Toast.LENGTH_SHORT)
                            .show()
                    }
            }
                dialogo?.setNegativeButton("No") { dialogInterface, i ->
                    dialogInterface.dismiss()
            }

            val alertDialog: AlertDialog = dialogo?.create()!!
            alertDialog.setCancelable(false)
            alertDialog.show()

        }
    }
