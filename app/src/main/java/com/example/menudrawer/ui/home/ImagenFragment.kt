package com.example.menudrawer.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.view.menu.MenuView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.menudrawer.R
import com.example.menudrawer.adapter.ImagenAdapter
import com.example.menudrawer.adapter.ProductoAdapter
import com.example.menudrawer.databinding.FragmentImagenBinding
import com.example.menudrawer.imagenAmpliada
import com.example.menudrawer.provider.Producto
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.item_imagen.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.ArrayList
import kotlinx.coroutines.tasks.await


class ImagenFragment : Fragment() {


    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth

    private var _binding: FragmentImagenBinding? = null
    private val binding get() = _binding!!

    val imageRef = Firebase.storage.reference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        ViewModelProvider(this)[HomeViewModel::class.java]

        _binding = FragmentImagenBinding.inflate(inflater, container, false)

        listaFicheros()
        return binding.root
    }

    private fun listaFicheros() = CoroutineScope(Dispatchers.IO).launch {
        try {
            firebaseAuth = FirebaseAuth.getInstance()
            val user = firebaseAuth.currentUser
            val email = user?.email

            val images = imageRef.child(email.toString()).listAll().await()
            val imageUrls = mutableListOf<String>()
            //recorremos un for con todas las imagenes del usuario y nos quedamos con los url
            for(image in images.items){
                val url = image.downloadUrl.await().toString()
                imageUrls.add(url)
            }
            withContext(Dispatchers.Main) {
                val imageAdapter = ImagenAdapter(context!!, imageUrls)
                binding.recyclerImagen.apply {
                    adapter = imageAdapter

                    binding.recyclerImagen.layoutManager = GridLayoutManager(context, 2)
                }
            }
        }catch (e: Exception){
            withContext(Dispatchers.Main){
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
            }
        }
    }


}