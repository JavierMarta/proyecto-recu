package com.example.menudrawer.ui.home

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.menudrawer.R
import com.example.menudrawer.adapter.TicketAdapter
import com.example.menudrawer.adapter.archiveAdapter
import com.example.menudrawer.databinding.FragmentArchivadosBinding
import com.example.menudrawer.databinding.FragmentTicketsBinding
import com.example.menudrawer.provider.Ticket
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*


class ArchivadosFragment : Fragment() {

    private var _binding: FragmentArchivadosBinding? = null
    private val binding get() = _binding!!

    private lateinit var archiveArrayList: ArrayList<Ticket>
    private lateinit var archiveAdapter: archiveAdapter

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        ViewModelProvider(this)[HomeViewModel::class.java]

        _binding = FragmentArchivadosBinding.inflate(inflater, container, false)

        binding.recyclerArchivados.layoutManager = LinearLayoutManager(context)
        binding.recyclerArchivados.setHasFixedSize(true)

        archiveArrayList = arrayListOf()

        archiveAdapter = archiveAdapter(requireContext(),archiveArrayList)
        binding.recyclerArchivados.adapter = archiveAdapter

        EventChangeListener()

        return binding.root
    }

    private fun EventChangeListener() {
        try {

            db = FirebaseFirestore.getInstance()
            firebaseAuth = FirebaseAuth.getInstance()

            val user = firebaseAuth.currentUser
            val email = user?.email

            db.collection("users").document(email.toString()).collection("archivados")
                .addSnapshotListener(object : EventListener<QuerySnapshot> {
                    override fun onEvent(
                        value: QuerySnapshot?,
                        error: FirebaseFirestoreException?
                    ) {

                        if (error != null) {

                            Log.e("Firestore error", error.message.toString())
                            return

                        }

                        for (dc: DocumentChange in value?.documentChanges!!) {

                            if (dc.type == DocumentChange.Type.ADDED) {

                                archiveArrayList.add(
                                    Ticket(
                                        dc.document.get("id").toString(),
                                        dc.document.get("fecha").toString(),
                                        dc.document.get("tienda").toString(),
                                        dc.document.get("precioTotal").toString()
                                    )
                                )

                            }
                        }

                        archiveAdapter.notifyDataSetChanged()

                    }
                })
        } catch (e: FirebaseFirestoreException) {
            Toast.makeText(activity, "Error con la base de datos", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Toast.makeText(activity, "Error ", Toast.LENGTH_SHORT).show()
        }
    }

}