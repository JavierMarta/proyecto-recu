package com.example.menudrawer.ui.home

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.menudrawer.SwipeGesture
import com.example.menudrawer.adapter.ProductoAdapter
import com.example.menudrawer.databinding.FragmentProductosBinding
import com.example.menudrawer.provider.Producto
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import java.util.*


class ProductosFragment : Fragment() {

    private lateinit var productoArrayList: ArrayList<Producto>
    private lateinit var productoAdapter: ProductoAdapter


    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth

    private var _binding: FragmentProductosBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        ViewModelProvider(this)[HomeViewModel::class.java]

        _binding = FragmentProductosBinding.inflate(inflater, container, false)



        binding.recyclerProductos.layoutManager = LinearLayoutManager(context)
        binding.recyclerProductos.setHasFixedSize(true)


        productoArrayList = arrayListOf()
        productoAdapter = ProductoAdapter(requireContext(),productoArrayList)
        binding.recyclerProductos.adapter = productoAdapter

        EventChangeListener()
        SwipeGestureRecycler()
        buscar()
        getData()

        return binding.root
    }

    private fun getData(){

        productoAdapter.setOnItemClickListener(object : ProductoAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {} })
    }

    private fun SwipeGestureRecycler() {



        val swipegesture = object : SwipeGesture(context) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                when (direction) {


                    ItemTouchHelper.LEFT ->  {

                        productoAdapter.deleteItem(viewHolder.adapterPosition)

                    }

                    ItemTouchHelper.RIGHT -> {

                        val archiveItem = productoArrayList[viewHolder.absoluteAdapterPosition]
                        productoAdapter.archiveItem(viewHolder.absoluteAdapterPosition)
                        productoAdapter.addItem(productoArrayList.size, archiveItem)

                    }
                }
            }

        }

        val touchHelper = ItemTouchHelper(swipegesture)
        touchHelper.attachToRecyclerView(binding.recyclerProductos)

    }
    //Bloque Mostrar Datos dentro del Recycler Productos
    private fun EventChangeListener() {

        try{
        db = FirebaseFirestore.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()

        val user = firebaseAuth.currentUser
        val email = user?.email

        db.collection("users").document(email.toString()).collection("productos")
            .addSnapshotListener(object : EventListener<QuerySnapshot> {
                override fun onEvent(
                    value: QuerySnapshot?,
                    error: FirebaseFirestoreException?
                ) {

                    if (error != null) {

                        Log.e("Firestore error", error.message.toString())
                        return

                    }

                    for (dc: DocumentChange in value?.documentChanges!!) {

                        if (dc.type == DocumentChange.Type.ADDED) {

                            productoArrayList.add(
                                Producto(
                                    dc.document.get("id").toString(),
                                    dc.document.get("idTicket").toString(),
                                    dc.document.get("producto").toString(),
                                    dc.document.get("precio").toString(),
                                    dc.document.get("unidades").toString()
                                )
                            )

                        }
                    }

                    productoAdapter.notifyDataSetChanged()

                }
            })

    }catch (e: FirebaseFirestoreException) {
            Toast.makeText(activity, "Error con la base de datos", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Toast.makeText(activity, "Error ", Toast.LENGTH_SHORT).show()
        }
    }

    fun buscar(){

        var search = binding.searchView

        if( search != null){
            search.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(consulta: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(texto: String?): Boolean {
                    if (texto!!.isNotEmpty()) {

                        var letra = texto.lowercase(Locale.getDefault())

                        var searchArrayList: ArrayList<Producto> = arrayListOf()

                        for (i in productoArrayList.indices) {
                            if (productoArrayList[i].producto!!.contains(letra) || productoArrayList[i].precioProducto!!.contains(letra))
                            {
                                searchArrayList.add(
                                    Producto(
                                        productoArrayList[i].id,
                                        productoArrayList[i].idTicket,
                                        productoArrayList[i].producto,
                                        productoArrayList[i].precioProducto,
                                        productoArrayList[i].udsProducto
                                    )
                                )
                            }
                            binding.recyclerProductos.adapter = ProductoAdapter(context!!,searchArrayList)
                            binding.recyclerProductos.adapter!!.notifyDataSetChanged()
                        }
                    }
                    else
                    {
                        productoAdapter = ProductoAdapter(context!!, productoArrayList)
                        binding.recyclerProductos.adapter = productoAdapter
                        productoAdapter.notifyDataSetChanged()
                    }
                    return false
                }
            })
        }

    }

}