package com.example.menudrawer.ui.home

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.TextView

import android.widget.Toast
import com.example.menudrawer.MainActivity
import com.example.menudrawer.R
import com.example.menudrawer.ScannerActivity
import com.example.menudrawer.databinding.FragmentEstadisticasBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.getField
import java.text.SimpleDateFormat
import java.util.*

class EstadisticasFragment : Fragment(), RadioGroup.OnCheckedChangeListener {

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth

    private var _binding: FragmentEstadisticasBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        _binding = FragmentEstadisticasBinding.inflate(inflater, container, false)

        binding.rbGroup.setOnCheckedChangeListener(this)

        db = FirebaseFirestore.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()

        buscarProducto()

        return binding.root
    }

    private fun buscarProducto() {
        try{

            val user = firebaseAuth.currentUser
            val email = user?.email

            binding.tiBuscar.setOnClickListener {


                val pBuscar = binding.tiBuscar.text.toString()
                var nCompras = 0

                db.collection("users").document(email.toString()).collection("productos")
                    .whereEqualTo("producto", pBuscar).get().addOnSuccessListener { documents ->

                        for (document in documents) {
                            var nProductos = document.getDouble("unidades")!!.toInt()

                            nCompras += nProductos
                        }

                        val dialog = AlertDialog.Builder(context)
                            .setTitle("Producto")
                            .setMessage("Has comprado $nCompras $pBuscar")
                            .setNegativeButton("Salir") { view, _ ->
                                view.dismiss()
                            }
                            .setCancelable(false)
                            .create()

                        dialog.show()

                    }
            }

    }catch (e: NumberFormatException) {
            val intent = Intent(context, TicketsFragment::class.java)
            startActivity(
                intent
            )
            Toast.makeText(context, "Error en el formato del numero", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            val intent = Intent(context, TicketsFragment::class.java)
            startActivity(
                intent
            )
            Toast.makeText(context, "Error ", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCheckedChanged(p0: RadioGroup?, radio: Int) {

        binding.tvZara.visibility = View.VISIBLE
        binding.tvPull.visibility = View.VISIBLE
        binding.tvBershka.visibility = View.VISIBLE
        binding.tvZara1.visibility = View.VISIBLE
        binding.tvZara2.visibility = View.VISIBLE
        binding.tvZara3.visibility = View.VISIBLE
        binding.tvPull1.visibility = View.VISIBLE
        binding.tvPull2.visibility = View.VISIBLE
        binding.tvPull3.visibility = View.VISIBLE
        binding.tvBershka1.visibility = View.VISIBLE
        binding.tvBershka2.visibility = View.VISIBLE
        binding.tvBershka3.visibility = View.VISIBLE
        binding.Q1.visibility = View.VISIBLE
        binding.Q2.visibility = View.VISIBLE
        binding.Q3.visibility = View.VISIBLE
        binding.Q4.visibility = View.VISIBLE
        binding.Q5.visibility = View.VISIBLE
        binding.Q6.visibility = View.VISIBLE
        binding.Q7.visibility = View.VISIBLE
        binding.Q8.visibility = View.VISIBLE
        binding.Q9.visibility = View.VISIBLE


        when (radio)
        {
            binding.rbTienda.id ->
            {
                consultasZara()
                consultasPull()
                consultasBershka()
            }

            binding.rbFecha.id ->
            {
                consultasFechaZara()
                consultasFechaPull()
                consultasFechaBershka()
            }
        }
    }

    private fun consultasZara() {
        try{

        val user = firebaseAuth.currentUser
        val email = user?.email

        var nCompras = 0
        var dineroTotal = 0.0
        var nProductos = 0
        var totalProductos = 0
        var nId = 0

        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "zara").get().addOnSuccessListener { documents ->

                for (document in documents)
                {
                    nCompras++
                    dineroTotal += document.getDouble("precioTotal")!!
                    nId = document.getField<Int>("id")!!.toInt()

                    db.collection("users").document(email.toString()).collection("productos")
                        .whereEqualTo("idTicket", nId).get().addOnSuccessListener { documents ->

                            for (document in documents) {

                                nProductos += document.getField<Int>("unidades")!!.toInt()
                                totalProductos = nProductos
                            }
                            binding.tvZara3.text = totalProductos.toString()
                        }

                }

                binding.Q1.text = "Nº Compras"
                binding.tvZara1.text = nCompras.toString()
                binding.Q2.text = "Dinero Gastado"
                binding.tvZara2.text = dineroTotal.toFloat().toString()
                binding.Q3.text = "Nº Productos"
            }
    }catch (e: NumberFormatException) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error en el formato del numero", Toast.LENGTH_SHORT).show()
    } catch (e: Exception) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error ", Toast.LENGTH_SHORT).show()
    }
}

    private fun consultasPull() {
try{
        val user = firebaseAuth.currentUser
        val email = user?.email

        var nCompras = 0
        var dineroTotal = 0.0
        var totalProductos = 0
        var nProductos = 0
        var nId = 0

        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "pull and bear").get().addOnSuccessListener { documents ->

                for (document in documents)
                {
                    nCompras++
                    dineroTotal += document.getDouble("precioTotal")!!
                    nId = document.getField<Int>("id")!!.toInt()

                    db.collection("users").document(email.toString()).collection("productos")
                        .whereEqualTo("idTicket", nId).get().addOnSuccessListener { documents ->

                            for (document in documents) {

                                nProductos += document.getField<Int>("unidades")!!.toInt()
                                totalProductos = nProductos
                            }
                            binding.tvPull3.text = totalProductos.toString()
                        }
                }

                binding.Q4.text = "Nº Compras"
                binding.tvPull1.text = nCompras.toString()
                binding.Q5.text = "Dinero Gastado"
                binding.tvPull2.text = dineroTotal.toFloat().toString()
                binding.Q6.text = "Nº Productos"
            }
    }catch (e: NumberFormatException) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error en el formato del numero", Toast.LENGTH_SHORT).show()
    } catch (e: Exception) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error ", Toast.LENGTH_SHORT).show()
    }
}

    private fun consultasBershka() {
        try{

        val user = firebaseAuth.currentUser
        val email = user?.email

        var nCompras = 0
        var dineroTotal = 0.0
        var totalProductos = 0
        var nProductos = 0
        var nId = 0

        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "bershka").get().addOnSuccessListener { documents ->

                for (document in documents)
                {
                    nCompras++
                    dineroTotal += document.getDouble("precioTotal")!!
                    nId = document.getField<Int>("id")!!.toInt()

                    db.collection("users").document(email.toString()).collection("productos")
                        .whereEqualTo("idTicket", nId).get().addOnSuccessListener { documents ->

                            for (document in documents) {

                                nProductos += document.getField<Int>("unidades")!!.toInt()
                                totalProductos = nProductos
                            }
                            binding.tvBershka3.text = totalProductos.toString()
                        }
                }

                binding.Q7.text = "Nº Compras"
                binding.tvBershka1.text = nCompras.toString()
                binding.Q8.text = "Dinero Gastado"
                binding.tvBershka2.text = dineroTotal.toFloat().toString()
                binding.Q9.text = "Nº Productos"
            }
    }catch (e: NumberFormatException) {
            val intent = Intent(context, TicketsFragment::class.java)
            startActivity(
                intent
            )
            Toast.makeText(context, "Error en el formato del numero", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            val intent = Intent(context, TicketsFragment::class.java)
            startActivity(
                intent
            )
            Toast.makeText(context, "Error ", Toast.LENGTH_SHORT).show()
        }
    }

    private fun consultasFechaZara(){

        try{
        val user = firebaseAuth.currentUser
        val email = user?.email

        val fechaActual = getCurrentDateTime()                          // fecha actual
        val fechaString = fechaActual.toString("dd/MM/yyyy")     // la convertimos en string con el formato que queremos

        val aux = fechaString.split("/")
        val mesActual = aux[1]                                          // mes actual
        val anioActual = aux[2]                                         // año actual

        var fecha = ""
        var nCompras = 0
        var totalGasto = 0.0
        var fechaUltima = ""

        val cal: Calendar = Calendar.getInstance()
        val sdf = SimpleDateFormat("dd/MM/yyyy")

        val fechaAux: Date = sdf.parse("01/01/2000")!!
        cal.time = fechaAux                                             // Fecha auxiliar antigua y la convertimos a milisegundos
        var fechaAuxMilis: Long = cal.timeInMillis

        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "zara").get().addOnSuccessListener { documents ->

                for (document in documents)
                {
                        fecha = document.getString("fecha")!!       // fecha del ticket

                        val aux = fecha.split("/")
                        val mesTicket = aux[1]                           // mes del ticket
                        val anioTicket = aux[2]

                    val fechaTicket: Date = sdf.parse(fecha)!!
                    cal.time = fechaTicket                              // fecha del ticket la convertimos a milisegundos
                    val fechaTicketMilis: Long = cal.timeInMillis

                    if (fechaTicketMilis >= fechaAuxMilis)
                    {
                        fechaAuxMilis = fechaTicketMilis                // comparamos las fechas en milisegundos
                        fechaUltima = fecha
                    }

                        if (mesActual == mesTicket && anioActual == anioTicket )            // comparamos mes actual y año actual
                        {
                            nCompras++                                                      // contamos numero de compras
                            totalGasto += document.getDouble("precioTotal")!!          // sumamos gasto total del mes
                        }
                }

                binding.Q1.text = "Ultima compra"
                binding.tvZara1.text = fechaUltima
                binding.Q2.text = "Compras ultimo mes"
                binding.tvZara2.text = nCompras.toString()
                binding.Q3.text = "Gasto ultimo mes"
                binding.tvZara3.text = totalGasto.toFloat().toString()

            }

    }catch (e: NumberFormatException) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error en el formato del numero", Toast.LENGTH_SHORT).show()
    } catch (e: Exception) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error ", Toast.LENGTH_SHORT).show()
    }
}


    private fun consultasFechaPull(){
        try{
        val user = firebaseAuth.currentUser
        val email = user?.email

        val fechaActual = getCurrentDateTime()
        val fechaString = fechaActual.toString("dd/MM/yyyy")

        val aux = fechaString.split("/")
        val mesActual = aux[1]
        val anioActual = aux[2]

        var fecha = ""
        var nCompras = 0
        var totalGasto = 0.0
        var fechaUltima = ""

        val cal: Calendar = Calendar.getInstance()
        val sdf = SimpleDateFormat("dd/MM/yyyy")

        val fechaAux: Date = sdf.parse("01/01/2000")!!
        cal.time = fechaAux
        var fechaAuxMilis: Long = cal.timeInMillis

        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "pull and bear").get().addOnSuccessListener { documents ->

                for (document in documents)
                {
                    fecha = document.getString("fecha")!!

                    val aux = fecha.split("/")
                    val mesTicket = aux[1]
                    val anioTicket = aux[2]

                    val fechaTicket: Date = sdf.parse(fecha)!!
                    cal.time = fechaTicket
                    val fechaTicketMilis: Long = cal.timeInMillis


                    if (fechaTicketMilis >= fechaAuxMilis)
                    {
                        fechaAuxMilis = fechaTicketMilis
                        fechaUltima = fecha
                    }

                    if (mesActual == mesTicket && anioActual == anioTicket )
                    {
                        nCompras++
                        totalGasto += document.getDouble("precioTotal")!!
                    }
                }

                binding.Q4.text = "Ultima compra"
                binding.tvPull1.text = fechaUltima
                binding.Q5.text = "Compras ultimo mes"
                binding.tvPull2.text = nCompras.toString()
                binding.Q6.text = "Gasto ultimo mes"
                binding.tvPull3.text = totalGasto.toFloat().toString()

            }
    }catch (e: NumberFormatException) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error en el formato del numero", Toast.LENGTH_SHORT).show()
    } catch (e: Exception) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error ", Toast.LENGTH_SHORT).show()
    }
}


    private fun consultasFechaBershka(){
        try{
        val user = firebaseAuth.currentUser
        val email = user?.email

        val fechaActual = getCurrentDateTime()
        val fechaString = fechaActual.toString("dd/MM/yyyy")

        val aux = fechaString.split("/")
        val mesActual = aux[1]
        val anioActual = aux[2]

        var fecha = ""
        var nCompras = 0
        var totalGasto = 0.0
        var fechaUltima = ""

        val cal: Calendar = Calendar.getInstance()
        val sdf = SimpleDateFormat("dd/MM/yyyy")

        val fechaAux: Date = sdf.parse("01/01/2000")!!
        cal.time = fechaAux
        var fechaAuxMilis: Long = cal.timeInMillis

        db.collection("users").document(email.toString()).collection("tickets")
            .whereEqualTo("tienda", "bershka").get().addOnSuccessListener { documents ->

                for (document in documents)
                {
                    fecha = document.getString("fecha")!!

                    val aux = fecha.split("/")
                    val mesTicket = aux[1]
                    val anioTicket = aux[2]

                    val fechaTicket: Date = sdf.parse(fecha)!!
                    cal.time = fechaTicket
                    val fechaTicketMilis: Long = cal.timeInMillis


                    if (fechaTicketMilis >= fechaAuxMilis)
                    {
                        fechaAuxMilis = fechaTicketMilis
                        fechaUltima = fecha
                    }

                    if (mesActual == mesTicket && anioActual == anioTicket )
                    {
                        nCompras++
                        totalGasto += document.getDouble("precioTotal")!!
                    }
                }

                binding.Q7.text = "Ultima compra"
                binding.tvBershka1.text = fechaUltima
                binding.Q8.text = "Compras ultimo mes"
                binding.tvBershka2.text = nCompras.toString()
                binding.Q9.text = "Gasto ultimo mes"
                binding.tvBershka3.text = totalGasto.toFloat().toString()

            }

    }catch (e: NumberFormatException) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error en el formato del numero", Toast.LENGTH_SHORT).show()
    } catch (e: Exception) {
        val intent = Intent(context, TicketsFragment::class.java)
        startActivity(
            intent
        )
        Toast.makeText(context, "Error ", Toast.LENGTH_SHORT).show()
    }
}

    fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {

        val formatter = SimpleDateFormat(format, locale)

        return formatter.format(this)
    }

    private fun getCurrentDateTime(): Date { return Calendar.getInstance().time }


}