package com.example.menudrawer.ui.home

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.SearchView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.menudrawer.R
import com.example.menudrawer.SwipeGesture
import com.example.menudrawer.adapter.TicketAdapter
import com.example.menudrawer.databinding.FragmentTicketsBinding
import com.example.menudrawer.provider.Ticket
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.firestore.EventListener
import java.util.*
import kotlin.collections.ArrayList


class TicketsFragment : Fragment() {

    private lateinit var ticketArrayList: ArrayList<Ticket>
    private lateinit var archiveArrayList: ArrayList<Ticket>

    private lateinit var ticketAdapter: TicketAdapter

    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth

    private var _binding: FragmentTicketsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        ViewModelProvider(this)[HomeViewModel::class.java]

        _binding = FragmentTicketsBinding.inflate(inflater, container, false)

        binding.recyclerTickets.layoutManager = LinearLayoutManager(context)
        binding.recyclerTickets.setHasFixedSize(true)

        ticketArrayList = arrayListOf()
        archiveArrayList = arrayListOf()

        ticketAdapter = TicketAdapter(requireContext(),ticketArrayList)
        binding.recyclerTickets.adapter = ticketAdapter

        EventChangeListener()
        SwipeGestureRecycler()
        buscar()
        getData()

        return binding.root
    }



    private fun getData(){

        ticketAdapter.setOnItemClickListener(object : TicketAdapter.onItemClickListener{
            override fun onItemClick(position: Int) {} })
    }

    private fun SwipeGestureRecycler() {

        val swipegesture = object : SwipeGesture(context) {

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                when (direction) {

                    ItemTouchHelper.LEFT ->  {

                        ticketAdapter.deleteItem(viewHolder.adapterPosition)

                    }

                    ItemTouchHelper.RIGHT -> {

                        val archiveItem = ticketArrayList[viewHolder.absoluteAdapterPosition]
                        ticketAdapter.deleteArchiveItem(viewHolder.absoluteAdapterPosition)
                        archivarItem(archiveArrayList.size, archiveItem)

                    }
                }
            }
        }

        val touchHelper = ItemTouchHelper(swipegesture)
        touchHelper.attachToRecyclerView(binding.recyclerTickets)

    }

    private fun archivarItem(i: Int, ticket: Ticket) {

        db = FirebaseFirestore.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()

        val user = firebaseAuth.currentUser
        val email = user?.email
        var idArch = 0

        archiveArrayList.add(i, ticket)

        db.collection("users").document(email.toString()).collection("archivados").get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    idArch += 1
                }
            }

        var fecha = ticket.fechaTicket
        var tienda = ticket.tiendaTicket
        var importe = ticket.importeTicket

        if (importe != null) {
            if (tienda != null) {
                db.collection("users").document(email.toString()).collection("archivados")
                    .document().set(
                        mapOf( "id" to idArch,"fecha" to fecha, "tienda" to tienda.lowercase(), "precioTotal" to importe.toDouble()))
            }
        }

    }

    private fun EventChangeListener() {
        try {

            db = FirebaseFirestore.getInstance()
            firebaseAuth = FirebaseAuth.getInstance()

            val user = firebaseAuth.currentUser
            val email = user?.email

            db.collection("users").document(email.toString()).collection("tickets")
                .addSnapshotListener(object : EventListener<QuerySnapshot> {
                    override fun onEvent(
                        value: QuerySnapshot?,
                        error: FirebaseFirestoreException?
                    ) {

                        if (error != null) {

                            Log.e("Firestore error", error.message.toString())
                            return

                        }

                        for (dc: DocumentChange in value?.documentChanges!!) {

                            if (dc.type == DocumentChange.Type.ADDED) {

                                ticketArrayList.add(
                                    Ticket(
                                        dc.document.get("id").toString(),
                                        dc.document.get("fecha").toString(),
                                        dc.document.get("tienda").toString(),
                                        dc.document.get("precioTotal").toString()
                                    )
                                )

                            }
                        }

                        ticketAdapter.notifyDataSetChanged()

                    }
                })
        } catch (e: FirebaseFirestoreException) {
            Toast.makeText(activity, "Error con la base de datos", Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
            Toast.makeText(activity, "Error ", Toast.LENGTH_SHORT).show()
        }
    }

    fun buscar(){

        var search = binding.searchView

        if( search != null){
            search.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(consulta: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(texto: String?): Boolean {
                    if (texto!!.isNotEmpty()) {

                        var letra = texto.lowercase(Locale.getDefault())

                        var searchArrayList: java.util.ArrayList<Ticket> = arrayListOf()

                        for (i in ticketArrayList.indices) {
                            if (ticketArrayList[i].tiendaTicket!!.contains(letra) || ticketArrayList[i].fechaTicket!!.contains(letra))
                            {
                                searchArrayList.add(
                                    Ticket(
                                        ticketArrayList[i].id,
                                        ticketArrayList[i].fechaTicket,
                                        ticketArrayList[i].tiendaTicket,
                                        ticketArrayList[i].importeTicket
                                    )
                                )
                            }
                            binding.recyclerTickets.adapter = TicketAdapter(context!!, searchArrayList)
                            binding.recyclerTickets.adapter!!.notifyDataSetChanged()
                        }
                    }
                    else
                    {
                        ticketAdapter = TicketAdapter(context!!, ticketArrayList)
                        binding.recyclerTickets.adapter = ticketAdapter
                        ticketAdapter.notifyDataSetChanged()
                    }
                    return false
                }
            })
        }

    }
}