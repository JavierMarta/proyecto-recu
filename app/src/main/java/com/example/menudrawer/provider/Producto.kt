package com.example.menudrawer.provider

data class Producto(
    var id: String? = null,
    var idTicket: String? = null,
    var producto: String? = null,
    var precioProducto: String? = null,
    var udsProducto: String? = null
)
