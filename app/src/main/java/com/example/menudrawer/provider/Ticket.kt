package com.example.menudrawer.provider

data class Ticket(
    var id: String? = null,
    var fechaTicket: String? = null,
    var tiendaTicket: String? = null,
    var importeTicket: String? = null
)
