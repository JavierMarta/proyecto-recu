package com.example.menudrawer

import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import com.example.menudrawer.databinding.ActivityGuardarImagenBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import org.intellij.lang.annotations.JdkConstants
import java.io.File
import java.io.OutputStream
import java.util.*
import kotlin.collections.HashMap

class guardarImagen : AppCompatActivity() {
        private lateinit var firebaseAuth: FirebaseAuth
        private val File2 = 1
        private val database = Firebase.database
        val myRef = database.getReference("user")



        private lateinit var binding: ActivityGuardarImagenBinding
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            binding = ActivityGuardarImagenBinding.inflate(layoutInflater)
            setContentView(binding.root)




            //Mostrar directamente la cámara
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).also {
                    it.resolveActivity(packageManager).also { component ->
                        createPhotoFile()
                        val photoUri: Uri =
                            FileProvider.getUriForFile(
                                this,
                                BuildConfig.APPLICATION_ID + ".fileprovider", file
                            )
                        it.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)

                    }
                }
                openCamera.launch(intent)


            //Si quiero repetir la imagen
             binding.abrirCamara.setOnClickListener {

            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).also {
                it.resolveActivity(packageManager).also { component ->
                    createPhotoFile()
                    val photoUri: Uri =
                        FileProvider.getUriForFile(
                            this,
                            BuildConfig.APPLICATION_ID + ".fileprovider", file
                        )
                    it.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
                }
            }
            openCamera.launch(intent)
             }

            binding.guardarGaleria.setOnClickListener {
                subirFichero()
                binding.guardarGaleria.isEnabled = false
                binding.abrirCamara.isEnabled = false

            }
        }


        private val openCamera =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == RESULT_OK) {
                    val bitmap = getBitmap()
                    binding.img.setImageBitmap(bitmap)
                    saveToGallery()

                }
            }
        private lateinit var file: File
        private fun createPhotoFile() {
            val dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
            file = File.createTempFile("IMG_${System.currentTimeMillis()}_", ".jpg", dir)
        }

    //Funcion para guardar imagen y sus funciones correspondientes
        private fun saveToGallery() {
            val content = createContent()
            val uri = save(content)
            clearContents(content, uri)
        }

        private fun createContent(): ContentValues {
            val fileName = file.name
            val fileType = "image/jpg"
            return ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                put(MediaStore.Files.FileColumns.MIME_TYPE, fileType)
                put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                put(MediaStore.MediaColumns.IS_PENDING, 1)

            }
        }

        private fun save(content: ContentValues): Uri {
            var outputStream: OutputStream?
            var uri: Uri?
            application.contentResolver.also { resolver ->
                uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, content)
                outputStream = resolver.openOutputStream(uri!!)
            }
            outputStream.use { output ->
                getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, output)
            }
            return uri!!
        }

        private fun clearContents(content: ContentValues, uri: Uri) {
            content.clear()
            content.put(MediaStore.MediaColumns.IS_PENDING,0)
            contentResolver.update(uri,content,null,null)
        }

        private fun getBitmap(): Bitmap {
            return BitmapFactory.decodeFile(file.toString())
        }
//Funcion Elegir foto desde la carpeta
    fun subirFichero(){

        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type= "image/*"
        startActivityForResult(intent,File2)
    }

//Función automática para guardar la imagen en la bbdd en la carpeta de STORAGE
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        firebaseAuth = FirebaseAuth.getInstance()
        val user = firebaseAuth.currentUser
        val email = user?.email
            if(requestCode == File2){
                if (resultCode == RESULT_OK){
                    val FileUri = data!!.data
                    val Folder: StorageReference = FirebaseStorage.getInstance().reference.child(email.toString())
                    val file_name: StorageReference = Folder.child("file" + FileUri!!.lastPathSegment)
                        file_name.putFile(FileUri).addOnSuccessListener {
                        file_name.downloadUrl.addOnSuccessListener { uri ->
                            val hashMap =
                                HashMap<String,String>()
                            hashMap["url"] = java.lang.String.valueOf(uri)
                            myRef.setValue(hashMap)
                    val intent = Intent(this, ScannerActivity::class.java)
                    startActivity(intent)
                    Toast.makeText(this,"Imagen guardada ",Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
    }
}